# Sommaire

## Définition de l'épreuve 

[ Note de service n° 2020-030 du 11-2-2020](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm)

### Partie écrite

Durée : 3 heures 30

[Exemples de sujets corrigés](https://pixees.fr/informatiquelycee/term/suj_bac/)

### Partie pratique

Durée : 1 heure

La [Banque Nationale de Sujets](https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi) donne les 40 sujets possibles pour le BAC 2022.

On peut s'entrainer avec les sections `À maitriser` et `Guidés` de <https://e-nsi.gitlab.io/pratique/>

## Adaptation

Une partie du programme de NSI ne figurera pas à l'épreuve du baccalauréat. On pourra donc la traiter après les épreuves.

[Note de service du 12-7-2021](https://www.education.gouv.fr/bo/21/Hebdo30/MENE2121274N.htm)

!!! warning "Hors programme BAC"

    À compter de la session 2022 du baccalauréat, les parties du programme de terminale qui ne pourront pas faire l'objet d'une évaluation lors de l'épreuve terminale écrite et pratique de l'enseignement de spécialité numérique et sciences informatiques de la classe de terminale de la voie générale définie dans la note de service n° 2020-030 du 11 février 2020 sont définies comme suit :

    1. Histoire de l'informatique
      
        - Évènements clés de l'histoire de l'informatique
    2. Structures de données
      
        - Graphes : structures relationnelles. Sommets, arcs, arêtes, graphes orientés ou non orientés
    3. Bases de données
      
        - Système de gestion de bases de données relationnelles
    4. Architectures matérielles, systèmes d'exploitation et réseaux
      
        - Sécurisation des communications
    5. Langages et programmation
      
        - Notions de programme en tant que donnée. Calculabilité, décidabilité
        - Paradigmes de programmation
    6. Algorithmique
      
        - Algorithmes sur les graphes
        - Programmation dynamique
        - Recherche textuelle

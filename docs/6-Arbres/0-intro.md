# Sommaire

Dans ce chapitre, on découvre :

* La notion d'arbre enraciné,
* la notion de tas (hors programme),
* la notion d'arbre binaire (dont ceux de recherche).

![Langues](images/carte-langues-europe.jpg)

> Source : [Feast Your Eyes on This Beautiful Linguistic Family Tree](https://www.mentalfloss.com/article/59665/feast-your-eyes-beautiful-linguistic-family-tree)
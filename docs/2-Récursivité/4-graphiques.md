# Dessins récursifs

## Arbre de Pythagore

Construire un programme Python pour recréer ce genre de figure.

![](assets/Pythagoras_tree_1_1_13_Summer.svg)

??? done "Solution"
    À venir

## Flocon de Von Koch

Construire un programme Python pour recréer ces étapes.

![](assets/Koch_anime.gif)

??? done "Solution"
    À venir

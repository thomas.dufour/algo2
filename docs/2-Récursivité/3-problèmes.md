# Problèmes en ligne

## France-IOI

### Exercices à reprendre

Quelques exercices peuvent être résolus à nouveau en utilisant une fonction récursive. C'est un très bon entrainement.

!!! example "Exemples"
    - [Construction d'une pyramide](http://www.france-ioi.org/algo/task.php?idChapter=644&idTask=1894)
    - [Socles pour statues](http://www.france-ioi.org/algo/task.php?idChapter=843&idTask=1934)
    - [Nombre d'amour](http://www.france-ioi.org/algo/task.php?idChapter=566&idTask=2415)
    - [Titres palindromiques](http://www.france-ioi.org/algo/task.php?idChapter=566&idTask=2417)

### Chapitre dédié

Résoudre les problèmes au sujet de [la récursivité sur FranceIOI](http://www.france-ioi.org/algo/chapter.php?idChapter=513).

??? done "Solution"
    [Plusieurs solutions ici](https://ens-fr.gitlab.io/france-ioi/N3/08-R%C3%A9cursivit%C3%A9/1-nombre_encadr%C3%A9/)


## Prologin

Quelques problèmes peuvent avoir une solution élégante avec la récursivité

!!! example "Exemples"
    - Niveau 1
        - 2004 [Netiquette](https://prologin.org/train/2004/semifinal/netiquette)
        - 2004 [Addition binaire](https://prologin.org/train/2004/semifinal/addition_binaire)
        - 2011 [Décryptage](https://prologin.org/train/2011/semifinal/decryptage)
    - Niveau 3
        - 2013 [_Reverse Alchemying_](https://prologin.org/train/2013/semifinal/reverse_alchemying)
    
# 🏡 Accueil

Les notions intermédiaires d'algorithmique,

- avec un peu plus de mathématiques en option,

- pour les élèves en spé NSI terminale et/ou en maths expertes.

![logo Python](./images/logo-python.svg){width=300}

Le cours utilise Python comme langage de programmation pour les exemples.

{{ terminal() }}

> Un grand remerciement à M. Vincent BOUILLOT pour le portage de [Pyodide](https://pyodide.org/en/stable/) vers MkDocs, cela permet d'utiliser Python encore plus simplement qu'avec [Basthon](https://basthon.fr/){ target=_blank }.


On utilisera plus de facilités du langage dans ce cours.

- On utilisera les f-string
- On utilisera la programmation orientée objet

Exercices d'entrainement <https://e-nsi.gitlab.io/-pratique/>


---

!!! info "À propos de ce site - RGPD"
    - Aucun _cookie_ n'est créé.
    - Il n'y a aucun lien vers des pisteurs.
        - Pas de polices Google qui espionne.
        - Pas de CDN malicieux.
    - Le langage Python est émulé par Pyodide sur votre propre machine.
        - Vous n'avez besoin de rien installer.
        - Strictement aucun code, aucune donnée personnelle n'est envoyée.
    - Adapté pour les PC, les tablettes et aussi les téléphones. 
    - Très peu gourmand en ressources, les pages sont légères.
    - Code source du contenu sous licence libre, sans utilisation commerciale possible.


## 🎓 Terminale NSI

Bienvenue en terminale.

- Relire de très nombreuses fois le cours et bien l'étudier, en particulier ses exercices. On pourra lire d'autres cours, présentés en bas.
- Travailler un maximum sur [France-IOI](http://www.france-ioi.org/) ; objectifs :
    - finir le niveau 3,
    - étudier les [solutions](https://ens-fr.gitlab.io/france-ioi/) de votre professeur,
    - avoir bien entamé le niveau 4 en particulier sur :
        - les arbres ;
        - les balayages de structures de données ;
        - les graphes.
- S'entrainer sur [Prologin](https://prologin.org/).
    - Retrouver les [corrigés](https://ens-fr.gitlab.io/prologin/) de votre professeur ainsi que des propositions d'autres élèves avec des commentaires.



## 🎲 Côté ludique

1. Un jeu : [Py-rates](https://py-rates.fr/index.html)
    - C'est du Python. Facile.

2. Un jeu : [CargoBot](http://www-verimag.imag.fr/~wack/CargoBot/)
    - Vous devez programmer un automate. Beaucoup de niveaux faciles, mais aussi des difficiles.

3. Un jeu : [RoboZZle](http://www.robozzle.com/beta/index.html?puzzle=-1)
    - Vous devez récupérer toutes les étoiles. Quelques niveaux faciles, beaucoup de niveaux qui feront réfléchir.

4. Un jeu : [Cube Composer](https://david-peter.de/cube-composer/)
    - Appliquer des fonctions pour transformer un objet en forme de cubes colorés en un autre. Quelques niveaux faciles, assez vite difficile.

## 📚 D'autres cours pour la NSI

### Dans l'établissement

- Cours de Dominique Filoé
    - [Réseaux et internet ](http://siingenieur.free.fr/nsi/partage/reseau/index.html)
    - [Modèle relationnel](http://siingenieur.free.fr/nsi/site_modelerelationnel/)
    - [Bases de données](http://siingenieur.free.fr/nsi/site_sql/)

### Ailleurs

- Cours de Gilles Lassus
    - [Première NSI](https://glassus.github.io/premiere_nsi/)
    - [Terminale NSI](https://glassus.github.io/terminale_nsi/)
- Cours de Fabrice Nativel
    - [Première NSI](https://fabricenativel.github.io/NSIPremiere/)
    - [Terminale NSI](https://fabricenativel.github.io/NSITerminale/)
- Cours de Vincent Bouillot
    - [Première NSI](https://ferney-nsi.gitlab.io/premiere/)
    - [Terminale NSI](https://ferney-nsi.gitlab.io/terminale/)
- [Cours de Fred Leleu](https://impytoyable.fr/index.html)
- Cours du lycée Angellier
    - [Première NSI](https://angellier.gitlab.io/nsi/premiere/)
    - [Terminale NSI](https://angellier.gitlab.io/nsi/terminale/)
- Cours de Vincent-Xavier Jumel
    - [Première NSI](https://lamadone.frama.io/informatique/premiere-nsi/index.html)
    - [Terminale NSI](https://lamadone.frama.io/informatique/terminale-nsi/index.html)
- Cours au lycée Lacassagne par Nicolas Buyle-Bodin
    - [Première](https://www.mathinfo.ovh/Premiere_NSI/00_Progression/index.html)
    - [Terminale](https://www.mathinfo.ovh/Terminale_NSI/00_Progression/index.html)
- Cours au lycée Saint Louis de Chateaulin
    - [Carnets Jupyter](https://github.com/saintlouis29/coursNSI/tree/3116e400f931c33c295b11a381e8fd553d0a04c5)
    

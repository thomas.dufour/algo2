# 📐 Mathématiques

## `pow`, pour la puissance (modulaire)

On a déjà vu comment calculer la puissance avec `#!python a ** n`,
 on peut aussi faire `#!python pow(a, n)`

```pycon
>>> 1337**42
198389692832016689128025814051186435469808931027259980194805041212767924492279648804437095653839742535006120000819629040274718649969
>>> pow(1337, 42)
198389692832016689128025814051186435469808931027259980194805041212767924492279648804437095653839742535006120000819629040274718649969
>>>
```

??? danger "Maths expertes"
    La fonction **native** `pow` permet d'utiliser un troisième paramètre optionnel `m`, pour donner la réponse modulo `m`.

    ```pycon
    >>> pow(1337, 42, 10**9)
    718649969
    >>>
    ```




## Flottants presque égaux

### Nombres proches dans l'absolu

(Rappel - Partie 1)

!!! warning "Pas de tests d'égalité entre flottants"

    ```pycon
    >>> 1/3 + 2/3 == 3/3
    True
    >>> 1/10 + 2/10 == 3/10
    False
    ```

    La raison étant que le stockage de $\frac1{10}$, $\frac2{10}$ et $\frac3{10}$ conduit à des approximations en nombres flottants qui ne sont pas égales aux décimaux d'origine. Les arrondis sur les opérations peuvent conduire à un écart qui donne un flottant plus ou moins « voisin ».


Plusieurs méthodes pour tester si deux flottants sont proches

=== "sans `round`"

    ```python
    x = ...  # un nombre inconnu
    y = ...  # un nombre inconnu

    if abs(x - y) < 10**-9:
        print("Les nombres x et y sont proches")
    ```

    Voilà une méthode correcte.

=== "avec un `round`"

    ```python
    x = ...  # un nombre inconnu
    y = ...  # un nombre inconnu

    if round(abs(x - y), 9) == 0:
        print("Les nombres x et y sont proches")
    ```

    On préfère ne faire aucun arrondi si c'est possible.

=== "avec deux `round`"

    ```python
    x = ...  # un nombre inconnu
    y = ...  # un nombre inconnu

    if round(x, 9) == round(y, 9):
        print("Les nombres x et y sont proches")
    ```

    On n'aime pas faire deux arrondis, quand un seul suffit.

!!! savoir "La méthode"
    Ici, la méthode à retenir c'est :
    
    - les flottants sont proches quand « l'écart absolu entre les deux flottants est faible ».

### Nombres relativement proches

!!! danger "Meilleure idée"
    :warning: Cette dernière méthode est presque correcte, mais ...

    - $10^{100}$ et $10^{100} + 1$ peuvent être considérés comme **relativement proches** l'un de l'autre ; mais pas par la fonction précédente qui renvoie `#!python False`. En effet, l'écart est de $1$, grand dans l'absolu, mais relativement faible.
    - $1.3×10^{-100}$ et $2.7×10^{-100}$ peuvent être considérés comme comparables, mais **pas relativement proches** ; cependant la fonction précédente renvoie `#!python True`. En effet, l'écart est de $1.4×10^{-100}$ qui est faible dans l'absolu, mais relativement comparable.

    Une troisième idée (écart relatif) est alors :

    ```python
    def sont_relativement_proches(x, y):
        """Renvoie un booléen
        L'écart relatif entre `x` et `y` est-il inférieur à 10^-9 ?
        """
        return abs(x - y) / max(abs(x), abs(y)) < 10**-9
    ```

    Cette solution est certes plus élaborée, 
    
    - elle répond correctement avec les deux contre-exemples précédents,
    - mais n'est pas satisfaisante si $x$ ou $y$ est nul ou presque.
        - Exemple 1, avec $x = 10^{-12}$ et $y=10^{-15}$, la fonction renvoie `False`. $x$ est mille fois plus grand que $y$. 
        - Exemple 2, avec $x = 1+10^{-12}$ et $y=1+10^{-15}$, la fonction renvoie `True`. Pourtant, il n'y a eu qu'une translation de $1$ entre l'exemple 1 et 2. **Quel résultat voulez-vous réellement ?**
        

    Une bonne méthode consiste à tolérer un écart relatif **ou** absolu.

    ```python
    return    abs(x - y) / max(abs(x), abs(y)) < 10**-9 \
           or abs(x - y) < 10**-9
    ```

    Il existe la fonction `isclose` dans le module `math` qui répond bien à cette problématique.

!!! tip "Bonne pratique"
    Suivant le contexte, on peut :

    - Utiliser une solution très simple qui suffira.
    - Utiliser une solution plus ou moins élaborée suivant le besoin.

    Mais il faut comprendre les risques d'erreurs en fonction du contexte.

    `#!python abs(x - y) < 10**-9` est souvent une bonne solution, et simple.

Pour en savoir plus : la [documentation officielle de Python](https://docs.python.org/fr/3/tutorial/floatingpoint.html)

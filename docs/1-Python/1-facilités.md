# 🪡 Facilités du langage

## Indices négatifs

truc[-1] ...

TODO

## Test dans un intervalle

a <= x <= b

## Fonctions utiles

En première, on essaie de vous faire réécrire les fonctions classiques pour les algorithmes simples de recherche du minimum, maximum, de la somme...

Vous pouvez désormais utiliser `min`, `max`, `sum` tant que vous savez les réécrire si on vous le demande.

zip, et plein de truc dans itertools...

TODO

## TODO en ajouter


## Les _f-string_

Une chaine de caractère (_string_) s'utilise dans les _docstring_ pour la **doc**umentation, mais aussi dans d'autres situations.

On peut avoir envie de placer des valeurs numériques dans une chaine de caractères.

La bonne pratique est d'utiliser des chaines **f**ormatées, d'où le nom _f-string_.

```pycon
>>> n = 42
>>> f"La réponse {n}, à la question Universelle..."
'La réponse 42, à la question Universelle...'
>>> a, b = 5, 7
>>> f"{a} + {b} = {a+b}"
'5 + 7 = 12'
>>> f"L'image de {n} par la fonction n -> 3n² -1 est {3*n*n - 1}"
"L'image de 42 par la fonction n -> 3n² -1 est 5291"
>>>
```

:warning: Il ne faut pas oublier de préfixer la chaine par `f` !

Voir <https://cis.bentley.edu/sandbox/wp-content/uploads/Documentation-on-f-strings.pdf>

TODO fstring

TODO évaluation paresseuse

- `#!py True or meme_pas_evalue()` ; réponse `#!py True` directement.
- `#!py False and meme_pas_evalue()` ; réponse `#!py False` directement.
- `#!py 0 * sera_evalue()` ; réponse $0$ ou Nan, ou erreur suivant la deuxième partie.

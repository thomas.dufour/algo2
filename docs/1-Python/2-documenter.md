# 📖 Documenter

## `help`, fonction d'aide

Pour afficher l'aide sur une fonction (ou un module) ; un réflexe à avoir !

=== "Example, l'aide sur `round`"

    ```pycon
    >>> help(round)
    Help on built-in function round in module builtins:

    round(number, ndigits=None)
        Round a number to a given precision in decimal digits.
        
        The return value is an integer if ndigits is omitted or None.  Otherwise
        the return value has the same type as the number.  ndigits may be negative.
    (END)
    ```

=== "Example, l'aide sur une fonction personnelle"

    ```pycon
    >>> help(collatz)
    Help on function collatz in module __main__:

    collatz(n)
        Renvoie la suite de Syracuse
        ayant `n` comme terme initial.
    ```

    Sous réserve d'avoir écrit la _docstring_ de sa fonction personnelle !

Les _docstring_ prennent ici tout leur sens !


🛰️ La fonction `help` est en particulier utile quand on travaille
 avec des modules écrits par d'autres personnes. Inutile d'aller étudier leur code si la _docstring_ est bien écrite.


## PEP 8 - Bonnes pratiques de codage


!!! info "PEP"
    PEP signifie _**P**ython **E**nhancement **P**roposal_, et il y en a plusieurs. Un PEP est un document qui décrit les nouvelles fonctionnalités proposées pour Python et documente les aspects de Python, comme la conception et le style, pour la communauté.

!!! example "Définitions"
    - **Tiret bas** : Connu en anglais comme *underscore*, on trouve ce caractère avec la touche du clavier <kbd>8_\\</kbd>
    - **_snake_case_** : Le [*snake case*](https://fr.wikipedia.org/wiki/Snake_case) est une convention typographique en informatique consistant à écrire des ensembles de mots, généralement, en minuscules en les séparant par des tirets bas.
    - _ **camel case**_ : Le *camel case* consiste à mettre en majuscule les premières lettres de chaque mot, sans séparer par un tiret bas. On l'utilise pour la définition d'une classe.


**PEP 8**, parfois orthographié PEP8 ou PEP-8, est **un document** qui fournit des directives et des meilleures pratiques sur la façon d'écrire du code Python. Il a été écrit en 2001 par Guido van Rossum, Barry Varsovie et Nick Coghlan. L'objectif principal de PEP 8 est d'améliorer la lisibilité et la cohérence du code Python.

### Les identifiants

1. On utilisera le *snake_case* pour toutes les variables, sauf :

    + pour les `class` que l'on nomme en *camel case*,
    + pour les constantes, en majuscule.

2. On donne des identifiants qui ont un sens.


=== "mal"

    ```python
    x = 'Jean Dupont'
    y, z = x.split()
    print(y, z, sep=', ')
    ```

=== "mieux"

    ```python
    texte = 'Jean Dupont'
    prénom, nom = texte.split()
    print(prénom, nom, sep=', ')
    ```


### Aération du code

* L'indentation doit être de **4** espaces.
* Une ligne vide après une définition de fonction.
* Deux lignes vides après une définition de classe.
* Une ligne vide pour séparer deux étapes marquées d'un groupe d'instructions.
* Une ligne vide pour aérer chaque *doctest*.
* Maximum 79 caractères de code par ligne.
    * Une ligne trop longue peut être coupée sans problème **à l'intérieur** de parenthèses, crochets ou accolades ; on parle de continuation implicite.
    * Sinon, on peut toujours couper une ligne avec `\`.
    * La continuation implicite est **à privilégier** dès que possible !

```python
def ma_fonction(argument_1,
                argument_2, argument_3, argument_4):
    pass

ma_fonction(22, 23, 24, 25) + \
    ma_fonction(34, 35, 36, 37)
```

### Aération des opérateurs

On met de l'espace autour des opérateurs binaires, sauf

+ pour les paramètres par défaut des fonctions,
+ pour les opérations prioritaires
+ pour `:` dans les définitions de tranches

=== "mal"

    ```python
    x = a*b+c
    if x > 5 and x % 2 == 0:
        print('x est plus grand que 5 et divisible par 2.')
    ```

=== "moyen"

    ```python
    x = a * b + c
    if x>5 and x%2==0:
            print('x est plus grand que 5 et divisible par 2.')
    ```

=== "bien"

    ```python
    x = a*b + c
    if (x > 5) and (x % 2 == 0):
            print('x est plus grand que 5 et divisible par 2.')
    ```

On met une espace : 

* juste après une virgule

```python
mon_tuple = (4, 11, 25)
```

On **ne met pas** d'espace

* juste après une parenthèse ouvrante (ou crochet ou accolade)
* juste avant une parenthèse fermante (idem).

On ne place jamais d'espace en fin de ligne.

### Un peu plus encore

Inspiré de ce [document](https://www.codeflow.site/fr/article/python-pep8), vous retrouverez plus de détails.

```python
# Not recommended
my_list = []
if not len(my_list):
    print('my_list is empty!')
```

En Python, toute liste, chaine ou tuple **vide** est transypée implicitement à `False`. Nous pouvons donc proposer une alternative plus simple à ce qui précède :

```python
# Recommended
my_list = []
if not my_list:
    print('my_list is empty!')
```

Alors que les deux exemples afficheront que la liste est vide (en anglais), la deuxième option est plus simple, donc PEP 8 l'encourage.

> Contrairement au PEP 8, en NSI nous préférons éviter le transtypage implicite, donc **une autre méthode est recommandée**. Les deux précédentes sont donc incorrectes pour nous.

```python
# La bonne méthode
if ma_liste == []:
    print("ma_liste est vide !")

# ou alors
if ma_liste != []:
    print("ma_liste n'est pas vide !")
```


TODO doctest


class Pile():
    def __init__(self, données=None):
        self.__données = données
    
    def est_vide(self):
        return self.__données is None

    def empile(self, élément):
        queue = self.__données
        tête = élément
        self.__données = (tête, queue)
    
    def dépile(self):
        if self.est_vide():
            raise ValueError('Pile vide')
        tête, queue = self.__données
        self.__données = queue
        return tête

if __name__ == '__main__':
    # tests
    test = Pile()

    # test est_vide
    assert test.est_vide()

    # test un empile/dépile
    test.empile(42)
    élément = test.dépile()
    assert élément == 42
    assert test.est_vide()

    # test plusieurs empile / puis plusieurs dépile
    premiers = [2, 3, 5, 7, 11]

    for élément in premiers:
        test.empile(élément)
    assert [test.dépile()
            for _ in range(len(premiers))] == premiers[::-1]
    assert test.est_vide()
    

class Pile():
    def __init__(self):
        self.données = []

    def __str__(self) -> str:
        return str(self.données)

    def est_vide(self) -> bool:
        return self.données == []

    def empile(self, valeur):
        self.données.append(valeur)
    
    def dépile(self):
        if self.est_vide():
            raise ValueError('Pile vide')
        return self.données.pop()

if __name__ == '__main__':
    # tests
    test = Pile()

    # test est_vide
    assert test.est_vide()

    # test un empile/dépile
    test.empile(42)
    élément = test.dépile()
    assert élément == 42
    assert test.est_vide()

    # test plusieurs empile / puis plusieurs dépile
    premiers = [2, 3, 5, 7, 11]

    for élément in premiers:
        test.empile(élément)
    assert [test.dépile()
            for _ in range(len(premiers))] == premiers[::-1]
    assert test.est_vide()
    

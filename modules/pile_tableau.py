class Pile():
    """
    Une classe pile, de taille maximale 'taille_max',
    implémentée avec les données dans un tableau.
    """

    def __init__(self, taille_max: int):
        self.taille_max = taille_max
        self.données = [None] * taille_max  # un tableau
        self.taille = 0

    def est_vide(self) -> bool:
        return self.taille == 0

    def empile(self, élément):
        """Ajoute `élément` au sommet de la pile.
        """
        if self.taille == self.taille_max:
            raise ValueError('Pile pleine')
        self.données[self.taille] = élément
        self.taille += 1
    
    def dépile(self):
        """Enlève et renvoie l' `élément` du sommet de la pile.
        """
        if self.est_vide():
            raise ValueError('Pile vide')
        self.taille -= 1
        élément = self.données[self.taille]
        self.données[self.taille] = None  # optionnel
        return élément

if __name__ == '__main__':
    # tests
    test = Pile(256)  # taille de 256 éléments

    # test est_vide
    assert test.est_vide()

    # test un empile/dépile
    test.empile(42)
    élément = test.dépile()
    assert élément == 42
    assert test.est_vide()

    # test plusieurs empile / puis plusieurs dépile
    premiers = [2, 3, 5, 7, 11]

    for élément in premiers:
        test.empile(élément)
    assert [test.dépile()
            for _ in range(len(premiers))] == premiers[::-1]
    assert test.est_vide()
    
